\newpage
\vspace*{.35\textheight}
{\centering \lettrine[lines=3,loversize=.45]{\color{gray}T}{\kern-12pt{o}} my family, and to \lettrine[lines=5,loversize=.15]{\color{gray}J}{\kern-8pt{ackie}}\par}
\newpage
\vspace*{.35\textheight}
\begin{center}{\Huge\color{gray}λ}\end{center}
\tableofcontents{} 
\addchap[Preface]{preface}
This book is intended for anyone who wants to become a better Lisp programmer.
It assumes some familiarity with Lisp, but not necessarily extensive programming
experience. The first few chapters contain a fair amount of review. I hope that
these sections will be interesting to more experienced Lisp programmers as well,
because they present familiar subjects in a new light.

It's difficult to convey the essence of a programming language in one sentence,
but John Foderaro has come close:

\begin{quote}
  Lisp is a programmable programming language.
\end{quote}
There is more to Lisp than this, but the ability to bend Lisp to one's will is a
large part of what distinguishes a Lisp expert from a novice. As well as writing
their programs down toward the language, experienced Lisp programmers build the
language up toward their programs. This book teaches how to program in the
bottom-up style for which Lisp is inherently well-suited.

\section*{bottom-up design}

Bottom-up design is becoming more important as software grows in complexity.
Programs today may have to meet specifications which are extremely complex, or
even open-ended. Under such circumstances, the traditional top-down method
sometimes breaks down. In its place there has evolved a style of programming
quite different from what is currently taught in most computer science courses:
a bottom-up style in which a program is written as a series of layers, each one
acting as a sort of programming language for the one above. X Windows and \TeX\
are examples of programs written in this style.

The theme of this book is twofold: that Lisp is a natural language for programs
written in the bottom-up style, and that the bottom-up style is a natural way to
write Lisp programs. \emph{On Lisp} will thus be of interest to two classes of readers.
For people interested in writing extensible programs, this book will show what
you can do if you have the right language. For Lisp programmers, this book
offers a practical explanation of how to use Lisp to its best advantage.

The title is intended to stress the importance of bottom-up programming in
Lisp. Instead of just writing your program in Lisp, you can write your own
language on Lisp, and write your program in that.

It is possible to write programs bottom-up in any language, but Lisp is the most
natural vehicle for this style of programming. In Lisp, bottom-up design is not
a special technique reserved for unusually large or difficult programs. Any
substantial program will be written partly in this style. Lisp was meant from
the start to be an extensible language. The language itself is mostly a
collection of Lisp functions, no different from the ones you define
yourself. What's more, Lisp functions can be expressed as lists, which are Lisp
data structures. This means you can write Lisp functions which generate Lisp
code.

A good Lisp programmer must know how to take advantage of this possibility.  The
usual way to do so is by defining a kind of operator called a macro. Mastering
macros is one of the most important steps in moving from writing correct Lisp
programs to writing beautiful ones. Introductory Lisp books have room for no
more than a quick overview of macros: an explanation of what macros are, together
with a few examples which hint at the strange and wonderful things you can do
with them. Those strange and wonderful things will receive special attention
here.  One of the aims of this book is to collect in one place all that people
have till now had to learn from experience about macros.

Understandably, introductory Lisp books do not emphasize the differences between
Lisp and other languages. They have to get their message across to students who
have, for the most part, been schooled to think of programs in Pascal terms. It
would only confuse matters to explain that, while \mclin{defun} looks like a procedure
definition, it is actually a program-writing program that generates code which
builds a functional object and indexes it under the symbol given as the first
argument.

One of the purposes of this book is to explain what makes Lisp different from
other languages. When I began, I knew that, all other things being equal, I
would much rather write programs in Lisp than in \textsc{c} or Pascal or Fortran. I knew
also that this was not merely a question of taste. But I realized that if I was
actually going to claim that Lisp was in some ways a better language, I had
better be prepared to explain why.

When someone asked Louis Armstrong what jazz was, he replied `If you have to ask
what jazz is, you'll never know'. But he did answer the question in a way: he
showed people what jazz was. That's one way to explain the power of Lisp---to
demonstrate techniques that would be difficult or impossible in other languages.
Most books on programming---even books on Lisp programming---deal with the kinds of
programs you could write in any language. \emph{On Lisp} deals mostly with the kinds of
programs you could only write in Lisp. Extensibility, bottom-up programming,
interactive development, source code transformation, embedded languages---this is
where Lisp shows to advantage.

In principle, of course, any Turing-equivalent programming language can do the
same things as any other. But that kind of power is not what programming
languages are about. In principle, anything you can do with a programming
language you can do with a Turing machine; in practice, programming a Turing
machine is not worth the trouble.

So when I say that this book is about how to do things that are impossible in
other languages, I don't mean `impossible' in the mathematical sense, but in the
sense that matters for programming languages. That is, if you had to write some
of the programs in this book in \textsc{c}, you might as well do it by writing a Lisp
compiler in \textsc{c} first. Embedding Prolog in \textsc{c}, for example---can you imagine the
amount of work that would take? \Cref{Chapter 24} shows how to do it in 180 lines of
Lisp. I hoped to do more than simply demonstrate the power of Lisp, though. I also
wanted to explain \emph{why} Lisp is different. This turns out to be a subtle
question---too subtle to be answered with phrases like `symbolic computation'.
What I have learned so far, I have tried to explain as clearly as I can.

\section*{plan of the book}

Since functions are the foundation of Lisp programs, the book begins with several chapters on functions. \Cref{Chapter 2} explains what Lisp functions are and the
possibilities they offer. \Cref{Chapter 3} then discusses the advantages of functional
programming, the dominant style in Lisp programs. \Cref{Chapter 4} shows how to use
functions to extend Lisp. Then \cref{Chapter 5} suggests the new kinds of abstractions
we can define with functions that return other functions. Finally, \cref{Chapter 6}
shows how to use functions in place of traditional data structures.

The remainder of the book deals more with macros than functions. Macros
receive more attention partly because there is more to say about them, and
partly because they have not till now been adequately described in
print. Chapters \ref{Chapter 7}--\ref{Chapter 10} form a complete tutorial on macro technique. By the end of
it you will know most of what an experienced Lisp programmer knows about
macros: how they work; how to define, test, and debug them; when to use macros
and when not; the major types of macros; how to write programs which generate
macro expansions; how macro style differs from Lisp style in general; and how
to detect and cure each of the unique problems that afflict macros.

Following this tutorial, Chapters \ref{Chapter 11}--\ref{Chapter 18} show some of the powerful abstractions you can build with macros. \Cref{Chapter 11} shows how to write the classic
macros---those which create context, or implement loops or conditionals. \cref{Chapter 12} explains the role of macros in operations on generalized
variables. \Cref{Chapter 13} shows how macros can make programs run faster by
shifting computation to compile-time. \Cref{Chapter 14} introduces anaphoric macros,
which allow you to use pronouns in your programs. \Cref{Chapter 15} shows how macros
provide a more convenient interface to the function-builders defined in \cref{Chapter 5}. \Cref{Chapter 16} shows how to use macro-defining macros to make Lisp write your
programs for you. \Cref{Chapter 17} discusses \mclin{read-macros}, and \cref{Chapter 18}, macros for destructuring.

With \cref{Chapter 19} begins the fourth part of the book, devoted to
embedded languages. \Cref{Chapter 19} introduces the subject by showing the same
program, a program to answer queries on a database, implemented first by an
interpreter and then as a true embedded language. \Cref{Chapter 20} shows how to
introduce into Common Lisp programs the notion of a continuation, an object
representing the remainder of a computation. Continuations are a very powerful
tool, and can be used to implement both multiple processes and nondeterministic
choice. Embedding these control structures in Lisp is discussed in \cref{Chapter 21,Chapter 22}, respectively. Nondeterminism, which allows you to write programs as if
they had foresight, sounds like an abstraction of unusual power. \Cref{Chapter 23,Chapter 24} present two embedded languages which show that nondeterminism lives up
to its promise: a complete \textsc{atn} parser and an embedded Prolog which combined
total about 200 lines of code.

The fact that these programs are short means nothing in itself. If you
resorted to writing incomprehensible code, there's no telling what you could do
in 200 lines. The point is, these programs are not short because they depend on
programming tricks, but because they're written using Lisp the way it's meant
to be used. The point of \cref{Chapter 23,Chapter 24} is not how to implement \textsc{atn}s in one
page of code or Prolog in two, but to show that these programs, when given
their most natural Lisp implementation, simply are that short. The embedded
languages in the latter chapters provide a proof by example of the twin points
with which I began: that Lisp is a natural language for bottom-up design, and
that bottom-up design is a natural way to use Lisp.

The book concludes with a discussion of object-oriented programming,
and particularly \textsc{clos}, the Common Lisp Object System. By saving this topic till
last, we see more clearly the way in which object-oriented programming is an
extension of ideas already present in Lisp. It is one of the many abstractions
that can be built on Lisp.

A chapter's worth of notes begins on page \pageref{notes-387}. The notes contain references,
additional or alternative code, or descriptions of aspects of Lisp not directly related
to the point at hand. Notes are indicated by a small circle in the outside margin,
like this. There is also an Appendix (page \pageref{appendix}) on packages.                                

Just as a tour of New York could be a tour of most of the world's cultures, a
study of Lisp as the programmable programming language draws in most of Lisp
technique. Most of the techniques described here are generally known in the Lisp
community, but many have not till now been written down anywhere. And some
issues, such as the proper role of macros or the nature of variable capture, are only
vaguely understood even by many experienced Lisp programmers.

\section*{examples}

Lisp is a family of languages. Since Common Lisp promises to remain a widely
used dialect, most of the examples in this book are in Common Lisp. The language
was originally defined in 1984 by the publication of Guy Steele's Common Lisp:
the Language (\textsc{cltl1}). This definition was superseded in 1990 by the publication
of the second edition (\textsc{cltl2}), which will in turn yield place to the forthcoming
\textsc{ansi} standard.

This book contains hundreds of examples, ranging from single expressions to
a working Prolog implementation. The code in this book has, wherever possible,
been written to work in any version of Common Lisp. Those few examples which
need features not found in \textsc{cltl1} implementations are explicitly identified in the
text. Later chapters contain some examples in Scheme. These too are clearly
identified.

The code is available by anonymous \textsc{ftp} from \url{endor.harvard.edu}, where
it's in the directory \texttt{pub/onlisp}. Questions and comments can be sent to
\url{onlisp@das.harvard.edu}.

\section*{acknowledgments}

While writing this book I have been particularly thankful for the
help of Robert Morris. I went to him constantly for advice and was
always glad I did. Several of the examples in this book are derived
from code he originally wrote, including the version of \mclin{for} on page
\pageref{for-127}, the version of \mclin{aand} on page \pageref{aand-191}, \mclin{match} on page \pageref{match-239}, the
breadth-first \mclin{true-choose} on page \pageref{notes-nondet-choose}, and the Prolog interpreter in
\cref{Section 24-2}. In fact, the whole book reflects (sometimes, indeed,
transcribes) conversations I've had with Robert during the past seven
years. (Thanks, \texttt{rtm}!)

I would also like to give special thanks to David Moon, who read large
parts of the manuscript with great care, and gave me very useful
comments. \Cref{Chapter 12} was completely rewritten at his suggestion, and the
example of variable capture on page \pageref{varcap-119} is one that he provided.

I was fortunate to have David Touretzky and Skona Brittain as the
technical reviewers for the book. Several sections were added or rewritten at
their suggestion. The alternative true nondeterministic \mclin{choice} operator on
page \pageref{true-choice-304} is based on a suggestion by David Toureztky.

Several other people consented to read all or part of the manuscript,
including Tom Cheatham, Richard Draves (who also rewrote \mclin{alambda} and \mclin{propmacro}
back in 1985), John Foderaro, David Hendler, George Luger, Robert Muller, Mark
Nitzberg, and Guy Steele.

I'm grateful to Professor Cheatham, and Harvard generally, for providing
the facilities used to write this book. Thanks also to the staff at Aiken Lab,
including Tony Hartman, Janusz Juda, Harry Bochner, and Joanne Klys.

The people at Prentice Hall did a great job. I feel fortunate to have
worked with Alan Apt, a good editor and a good guy. Thanks also to Mona
Pompili, Shirley Michaels, and Shirley McGuire for their organization and good
humor.

The incomparable Gino Lee of the Bow and Arrow Press, Cambridge, did the
cover. The tree on the cover alludes specifically to the point made on page \pageref{cover-27}.

This book was typeset using \LaTeX, a language written by Leslie Lamport
atop Donald Knuth's \TeX, with additional macros by L. A. Carr, Van Jacobson,
and Guy Steele. The diagrams were done with Idraw, by John Vlissides and Scott
Stanton. The whole was previewed with Ghostview, by Tim Theisen, which is built
on Ghostscript, by L. Peter Deutsch. Gary Bisbee of Chiron Inc. produced the
camera-ready copy.

I owe thanks to many others, including Paul Becker, Phil Chapnick, Alice
Hartley, Glenn Holloway, Meichun Hsu, Krzysztof Lenk, Arman Maghbouleh, Howard
Mullings, Nancy Parmet, Robert Penny, Gary Sabot, Patrick Slaney, Steve
Strassman, Dave Watkins, the Weickers, and Bill Woods.

Most of all, I'd like to thank my parents, for their example and
encouragement; and Jackie, who taught me what I might have learned if I had
listened to them.

I hope reading this book will be fun. Of all the languages I know, I like
Lisp the best, simply because it's the most beautiful. This book is about Lisp
at its lispiest. I had fun writing it, and I hope that comes through in the
text.

\hfill---Paul Graham
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
